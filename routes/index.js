var express = require('express');
var router = express.Router();
var fs=require('fs');
router.get('/streamvedio', function(req, res, next) {
  try 
  {
    const fileSize = fs.statSync("T20_Hilights.mp4").size;
    const range = req.headers.range;
    if (range) {
      const parts = range.replace(/bytes=/, "").split("-");
      const start = parseInt(parts[0], 10);
      const end = parts[1] 
        ? parseInt(parts[1], 10)
        : fileSize-1;
      const file = fs.createReadStream('T20_Hilights.mp4', {start, end});
      const head = {
        'Content-Range': `bytes ${start}-${end}/${fileSize}`,
        'Accept-Ranges': 'bytes',
        'Content-Length': (end-start)+1,
        'Content-Type': 'video/mp4'
      }
      res.writeHead(206, head);
      file.pipe(res);
    }
  }
  catch(exceoption)
  {
    next(exceoption);
  }
 
});
module.exports = router;
